<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Jadwal;
use App\Http\Resources\Jadwal as JadwalResource;

class JadwalController extends BaseController
{
    public function index()
    {
        $jadwal = Jadwal::all();
        return $this->sendResponse(JadwalResource::collection($jadwal), 'Jadwal ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'vaksin_id' => 'required',
            'rs_id' => 'required',
            'tanggal' => 'required',
            'jam' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $jadwal = new Jadwal();
        $jadwal->vaksin_id = $input['vaksin_id'];
        $jadwal->rs_id = $input['rs_id'];
        $jadwal->tanggal = $input['tanggal'];
        $jadwal->jam = $input['jam'];
        $jadwal->save();

        return $this->sendResponse(new JadwalResource($jadwal), 'Data Jadwal ditambahkan.');
    }

    public function show($id)
    {
        $jadwal = Jadwal::find($id);
        if (is_null($jadwal)) {
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new JadwalResource($jadwal), 'Data ditampilkan.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $jadwal = Jadwal::find($id);
        if (!is_null($jadwal)) {
            $validator = Validator::make($input, [
                'vaksin_id' => 'required',
                'rs_id' => 'required',
                'tanggal' => 'required',
                'jam' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $jadwal->vaksin_id = $input['vaksin_id'];
            $jadwal->rs_id = $input['rs_id'];
            $jadwal->tanggal = $input['tanggal'];
            $jadwal->jam = $input['jam'];
            $jadwal->save();
        }

        return $this->sendResponse(new JadwalResource($jadwal), 'Data updated.');
    }

    public function destroy($id)
    {
        $jadwal = Jadwal::find($id);
        if (!is_null($jadwal)) {
            $jadwal->delete();
        }
        // $jadwal->delete();

        return $this->sendResponse([], 'Data deleted.');
    }
}
