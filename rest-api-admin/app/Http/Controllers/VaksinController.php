<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Vaksin;
use App\Http\Resources\Vaksin as VaksinResource;

class VaksinController extends BaseController
{
    public function index()
    {
        $vaksin = Vaksin::all();
        return $this->sendResponse(VaksinResource::collection($vaksin),'Vaksin ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'jenis' => 'required'
        ]);
        
        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $vaksin = new Vaksin();
        $vaksin->jenis = $input['jenis'];
        $vaksin->save();

        return $this->sendResponse(new VaksinResource($vaksin), 'Data Vaksin ditambahkan.');
    }

    public function show($id)
    {
        $vaksin = Vaksin::find($id);
        if(is_null($vaksin)){
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new VaksinResource($vaksin), 'Data ditampilkan.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $rs = Vaksin::find($id);
        if (!is_null($rs)) {
            $validator = Validator::make($input, [
                'jenis' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $rs->jenis = $input['jenis'];
            $rs->save();
        }

        return $this->sendResponse(new VaksinResource($rs), 'Data updated.');
    }

    public function destroy($id)
    {
        $vaksin = Vaksin::find($id);
        if(!is_null($vaksin)){
            $vaksin->delete();
        }

        return $this->sendResponse([], 'Data deleted.');
    }
}
