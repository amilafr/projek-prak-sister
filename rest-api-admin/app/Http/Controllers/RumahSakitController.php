<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\RumahSakit;
use App\Http\Resources\RumahSakit as RumahSakitResource;

class RumahSakitController extends BaseController
{
    public function index()
    {
        $rs = RumahSakit::all();
        return $this->sendResponse(RumahSakitResource::collection($rs), 'RumahSakit ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nama' => 'required',
            'kota' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $rs = new RumahSakit();
        $rs->nama = $input['nama'];
        $rs->kota = $input['kota'];
        $rs->save();

        return $this->sendResponse(new RumahSakitResource($rs), 'Data RumahSakit ditambahkan.');
    }

    public function show($id)
    {
        $rs = RumahSakit::find($id);
        if (is_null($rs)) {
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new RumahSakitResource($rs), 'Data ditampilkan.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $rs = RumahSakit::find($id);
        if (!is_null($rs)) {
            $validator = Validator::make($input, [
                'nama' => 'required',
                'kota' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $rs->nama = $input['nama'];
            $rs->kota = $input['kota'];
            $rs->save();
        }

        return $this->sendResponse(new RumahSakitResource($rs), 'Data updated.');
    }

    public function destroy($id)
    {
        $rs = RumahSakit::find($id);
        if (!is_null($rs)) {
            $rs->delete();
        }

        return $this->sendResponse([], 'Data deleted.');
    }
}
