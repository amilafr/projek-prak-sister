<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Jadwal extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vaksin_id' => $this->vaksin_id,
            'rs_id' => $this->rs_id,
            'tanggal' => $this->tanggal,
            'jam' => $this->jam,
        ];
    }
}
