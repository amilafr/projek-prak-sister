<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vaksin_id')->unsigned();
            $table->integer('rs_id')->unsigned();
            $table->date('tanggal');
            $table->string('jam');
            $table->timestamps();

            // $table->foreign('vaksin_id')->references('id')->on('vaksins')->onDelete('CASCADE');
            // $table->foreign('rs_id')->references('id')->on('rumah_sakits')->onDelete('CASCADE');
        });

        // Schema::table('priorities', function ($table) {
        //     $table->foreign('vaksin_id')->references('id')->on('vaksins')->onDelete('CASCADE');
        //     $table->foreign('rs_id')->references('id')->on('rumah_sakits')->onDelete('CASCADE');
        // });

        Schema::table('jadwals', function($table) {
            $table->foreign('vaksin_id')->references('id')->on('vaksins');
            $table->foreign('rs_id')->references('id')->on('rumah_sakits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
    }
}
