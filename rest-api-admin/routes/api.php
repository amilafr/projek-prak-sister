<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\VaksinController;
use App\Http\Controllers\RumahSakitController;
use App\Http\Controllers\JadwalController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [AuthController::class, 'signin']);
Route::post('register', [AuthController::class, 'signup']);

// Route::middleware('auth:sanctum')->group(function () {
//     Route::resource('vaksin', VaksinController::class);
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('vaksin',[VaksinController::class,'index']);
Route::post('vaksin',[VaksinController::class,'create']);
Route::put('/vaksin/{id}',[VaksinController::class,'update']);
Route::delete('vaksin/{id}',[VaksinController::class,'destroy']);
Route::get('vaksin/{id}', [VaksinController::class, 'show']);

Route::get('rs',[RumahSakitController::class,'index']);
Route::post('rs',[RumahSakitController::class,'create']);
Route::put('/rs/{id}',[RumahSakitController::class,'update']);
Route::delete('rs/{id}',[RumahSakitController::class,'destroy']);
Route::get('rs/{id}', [RumahSakitController::class, 'show']);

Route::get('jadwal',[JadwalController::class,'index']);
Route::post('jadwal',[JadwalController::class,'create']);
Route::put('/jadwal/{id}',[JadwalController::class,'update']);
Route::delete('jadwal/{id}',[JadwalController::class,'destroy']);
Route::get('jadwal/{id}', [JadwalController::class, 'show']);

