<?php

namespace App\Http\Controllers;

use App\Daftar2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Daftar2 as Daftar2Resource;

class Daftar2Controller extends BaseController
{
    public function index()
    {
        $daftar2 = Daftar2::all();
        return $this->sendResponse(Daftar2Resource::collection($daftar2), 'Daftar2 ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nik' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'lahir' => 'required',
            'gender' => 'required',
            'penyakit' => 'required',
            'alergi' => 'required',
            'user_id' => 'required',
            'jadwalid' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $daftar2 = new Daftar2();
        $daftar2->nik = $input['nik'];
        $daftar2->nama = $input['nama'];
        $daftar2->alamat = $input['alamat'];
        $daftar2->lahir = $input['lahir'];
        $daftar2->gender = $input['gender'];
        $daftar2->penyakit = $input['penyakit'];
        $daftar2->alergi = $input['alergi'];
        $daftar2->user_id = $input['user_id'];
        $daftar2->jadwalid = $input['jadwalid'];
        $daftar2->save();

        return $this->sendResponse(new Daftar2Resource($daftar2), 'Data Daftar2 ditambahkan.');
    }

    public function show($id)
    {
        $daftar2 = Daftar2::find($id);
        if (is_null($daftar2)) {
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new Daftar2Resource($daftar2), 'Data ditampilkan.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $daftar2 = Daftar2::find($id);
        if (!is_null($daftar2)) {
            $validator = Validator::make($input, [
                'nik' => 'required',
                'nama' => 'required',
                'alamat' => 'required',
                'lahir' => 'required',
                'gender' => 'required',
                'penyakit' => 'required',
                'alergi' => 'required',
                'user_id' => 'required',
                'jadwalid' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $daftar2->nik = $input['nik'];
            $daftar2->nama = $input['nama'];
            $daftar2->alamat = $input['alamat'];
            $daftar2->lahir = $input['lahir'];
            $daftar2->gender = $input['gender'];
            $daftar2->penyakit = $input['penyakit'];
            $daftar2->alergi = $input['alergi'];
            $daftar2->user_id = $input['user_id'];
            $daftar2->jadwalid = $input['jadwalid'];
            $daftar2->save();
        }

        return $this->sendResponse(new Daftar2Resource($daftar2), 'Data updated.');
    }

    public function destroy($id)
    {
        $daftar2 = Daftar2::find($id);
        if (!is_null($daftar2)) {
            $daftar2->delete();
        }

        return $this->sendResponse([], 'Data deleted.');
    }
}
